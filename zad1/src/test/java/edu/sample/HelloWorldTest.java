package edu.sample;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTest {

    HelloWorld helloWorld = new HelloWorld();

    @Test
    public void test(){
        assertEquals("message() method returns wrong string", "Hello World!", helloWorld.message());
    }

}